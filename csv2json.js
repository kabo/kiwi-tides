#!/usr/bin/env node
/* eslint-env node */
/* eslint no-console: "off" */

const fs = require('mz/fs')
const neatcsv = require('neat-csv')
const dms = require('dms-conversion')

const csv_dir = 'csv'
const json_dir = 'public/json'

const flatten = arr => arr.reduce(
  (acc, val) => acc.concat(
    Array.isArray(val) ? flatten(val) : val
  ),
  []
)
const padLeft = (nr, n, str) => Array(n-String(nr).length+1).join(str||'0')+nr
const slugify = (text) => text.toString().toLowerCase()
  .replace(/\s+/g, '-')           // Replace spaces with -
  .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
  .replace(/\-\-+/g, '-')         // Replace multiple - with single -
  .replace(/^-+/, '')             // Trim - from start of text
  .replace(/-+$/, '')             // Trim - from end of text


const getFilenames = () => {
  return fs.readdir(`./${csv_dir}`)
}
const convertFile = async (filename) => {
  const raw = await fs.readFile(`./${csv_dir}/${filename}`, 'latin1')
  const data = await neatcsv(raw, {headers: ['dom', 'dow', 'month', 'year', 't1', 'h1', 't2', 'h2', 't3', 'h3', 't4', 'h4']})
  if (data.length < 1) {
    console.warn('no data', {filename, raw, data})
    return false
  }
  const latlng = [data[0].month, data[0].year]
  const name = filename.substr(0, filename.length-9)
  return {
    location: name,
    id: slugify(name),
    year: data[3].year,
    lnglat: latlng.reverse().map(dms.parseDms),
    tides: flatten(data.slice(3).map(row => {
      const result = []
      for (let i=1; i<=4; ++i) {
        const t = `t${i}`
        if (!row[t]) {
          break
        }
        const h = `h${i}`
        result.push({
          datetime: `${row.year}-${padLeft(row.month,2)}-${padLeft(row.dom,2)}T${row[t]}`,
          height: row[h]
        })
      }
      return result
    }))
  }
}
const writeFile = (data) => {
  return fs.writeFile(`./${json_dir}/${data.id}-${data.year}.json`, JSON.stringify(data))
}
const writeLocations = (data) => {
  const cmp = (a, b) => {
    return a.location.localeCompare(b.location)
  }
  const uniqLocations = data.filter((datum, i, a) => a.findIndex(d => d.id === datum.id) === i)
    .map(datum => {
      return {
        id: datum.id,
        location: datum.location,
        lnglat: datum.lnglat
      }
    })
    .sort(cmp)
  return fs.writeFile(`./${json_dir}/locations.json`, JSON.stringify(uniqLocations))
}

getFilenames()
  .then(files => {
    return Promise.all(files.map(filename => convertFile(filename)))
  })
  .then(data => {
    data = data.filter(d => !!d)
    const writes = data.map(datum => writeFile(datum))
    writes.push(writeLocations(data))
    return Promise.all(writes)
  })
  .then(() => console.log('done'))
  .catch(e => {
    console.error(e)
  })
