#!/usr/bin/env node
/* eslint-env node */
/* eslint no-console: "off" */

const fs = require('mz/fs')
const fetch = require('node-fetch')
const eachSeries = require('async/eachSeries')

const csv_dir = 'csv'
const years = ['2018', '2019', '2020']
const base = 'http://www.linz.govt.nz/sites/default/files/docs/hydro/tidal-info/tide-tables/maj-ports/csv/'
const portnames = [
  'Charleston',
  'Green Island',
  'Rocky Point',
  'Huruhi Harbour',
  'Raglan',
  'Pouto Point',
  'Mana',
  'Kawhia',
  'Spit Wharf',
  'Port Taranaki',
  'Oban',
  'Tauranga',
  'Port Chalmers',
  'Dunedin',
  'Flour Cask Bay',
  'Napier',
  'Oamaru',
  'Auckland',
  'Castlepoint',
  'Havelock',
  'Lyttelton',
  'Tarakohe',
  'Anawhata',
  'Paratutae Island',
  'Mapua',
  'Whangarei',
  'Bluff',
  'Opua',
  'Gisborne',
  'French Bay',
  'Nelson',
  'Kaiteriteri',
  'Port Ohope Wharf',
  'Wellington',
  'Westport',
  'Leigh',
  'Matiatia Bay',
  'Marsden Point',
  'Welcombe Bay',
  'Kaituna River',
  'Lottin Point',
  'Omokoroa',
  'Opotiki Wharf',
  'Onehunga',
  'Whakatane',
  'Whitianga',
  'Timaru',
  'Deep Cove',
  'North Cape (Otou)',
  'Whangaroa',
  'Ben Gunn Wharf',
  'Korotiti Bay',
  'Fresh Water Basin',
  'Waiorua Bay',
  'Kaikoura',
  'Sumner',
  'Picton',
  'Jackson Bay',
  'Whanganui River Entrance',
  'Man O\' War Bay'
]

const getFilesForYears = ({portnames, years}) => {
  return new Promise((resolve, reject) => {
    const portyears = []
    portnames.forEach(portname => {
      years.forEach(year => {
        portyears.push(`${portname.toLowerCase().replace(/\b\w/g, l => l.toUpperCase())} ${year}`)
      })
    })
    eachSeries(portyears, (portyear, next) => {
      const filepath = `${csv_dir}/${portyear}.csv`
      const url = `${base}${encodeURIComponent(portyear)}.csv`
      let fd
      fs.open(filepath, 'wx')
        .then(f => {
          fd = f
          console.log(`fetching ${url}`)
          return fetch(url)
        })
        .then(r => {
          if (r.ok) {
            const stream = fs.createWriteStream(null, {defaultEncoding: 'ascii', fd})
            return r.body.pipe(stream)
          }
          throw r
        })
        .then(() => {
          next()
        })
        .catch(e => {
          if (e.code && e.code === 'EEXIST') {
            console.log(`${portyear} already exists, skipping`)
            return next()
          }
          try {
            fs.unlinkSync(filepath)
          } catch (Error) {
          // unable to unlink, it's ok
          }
          if (!e.status) {
            console.error(e)
          }
          console.error(`Unable to fetch ${url}`, e.status)
          next()
        })
    })
  })
}
getFilesForYears({portnames, years})
  .catch(e => {
    console.error(e)
  })

