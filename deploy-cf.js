#!/usr/bin/env node
/* eslint-env node */
/* eslint no-console: "off" */

const argv = require('yargs')
.usage('Usage: $0 --domainname [domainname]')
.alias('n', 'domainname')
.describe('n', 'The domain used to serve the website')
.demandOption(['n'])
.help('h')
.alias('h', 'help')
.argv

const domainname = argv.domainname
let stackid

const fs = require('mz/fs')
const AWS = require('aws-sdk')
AWS.config.update({region: 'us-east-1'})
const cf = new AWS.CloudFormation()

fs.readFile('cf.template').then(template => {
  const params = {
    StackName: 'KiwiTides',
    TemplateBody: template,
    Parameters: [{
      ParameterKey: 'DomainName',
      ParameterValue: domainname
    }]
  }
  console.log('Creating stack...')
  return cf.createStack(params).promise()
})
.then(data => {
  stackid = data.StackId
  console.log(`Waiting for stack ${stackid} to complete (this is going to take a while)...`)
  return cf.waitFor('stackCreateComplete', {StackName: stackid}).promise()
})
.then(data => {
  //console.log(data)
  console.log('Stack created, finishing...')
  const bucketname = data.Stacks[0].Outputs.find(o => o.OutputKey === stackid).OutputValue
  return fs.writeFile('.deploy', `S3_BUCKET="${bucketname}"`)
})
.then(data => {
  console.log('Done!')
})
.catch(e => {
  console.error(e)
})


