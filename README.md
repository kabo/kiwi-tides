# Kiwi Tides

## Work in progress

- [x] Understand LINZ files
- [x] Convert LINZ files into JSON chunks
- [x] Redux & Immutable
- [x] App ( Tides ( LocationPicker, TimeNav, Chart, Table ) )
- [x] Mark current time in chart
- [x] Add script to deploy to s3
- [ ] Use moment timezone, don't assume user is in NZ
- [x] Lazy load tide data
- [x] Loading data spinner
- [x] Add more locations
- [ ] Make it purdy
- [ ] Nice time slider?
- [x] Nice map of locations
- [ ] Put all state in Redux, with each component providing it's own reducer for stuff that's only relevant for that component

Redux architecture:

```YAML
root:
  tideData:
    - location: name/id
      year: YYYY
      tides:
        - datetime: YYYY-MM-DD
          height: 0.2
        - datetime: YYYY-MM-DD
          height: 2.6
        - ...
    - ...
  locations:
    - id: locationId
      location: locationName
    - id: locationId
      location: locationName
    - ...
  locationId: locationId
  dateRange:
    - fromDate (YYYY-MM-DD)
    - toDate (YYYY-MM-DD)
  loadingLocations: true/false
  loadingTideData: true/false
```

## Deploying

Before you can deploy you need to set up the necessary infrastructure. Run `yarn deploy-cf -- [your-domain-name]` to create an S3 bucket for the website files and an S3 bucket for the logs, issue a https certificate and set up a CloudFront distribution. It is assumed that you are able to receive the emails sent by ACM (i.e. webmaster/admin/postmaster/administrator/hostmaster@[your-domain-name]) in order to approve the certificate. This command only needs to be run once. Be aware that it's going to take a while, at least 15 minutes for this to finish.

Once everything is set up, run `yarn deploy` to build and sync the new build to S3.

## To convert CSVs into JSON

Run `yarn csv2json`. It will scan all the CSVs in the `csv` folder and convert them into usable json files and place them in the `json` folder.

## License

Kiwi tides is licensed under the terms of the MIT license.

This work includes LINZ's data which are licensed by [Land Information New Zealand (LINZ)](http://www.linz.govt.nz/) for re-use under the [Creative Commons Attribution 4.0 International licence](http://creativecommons.org/licenses/by/4.0/).

