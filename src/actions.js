/*eslint no-console: ["warn", { allow: ["warn", "error"] }] */
export function init(locationId) {
  return {
    type: 'SET_STATE',
    state: {
      locationId: null,
      locations: [],
      tideData: [],
      dateRange: null,
      loadingLocations: false
    }
  }
}
export function setLocationId(locationId) {
  return {
    type: 'SET_LOCATION',
    locationId
  }
}
export function setLoadingLocations(status) {
  return {
    type: 'SET_LOADING_LOCATIONS',
    loadingLocations: status
  }
}
export function setLocations(locations) {
  return {
    type: 'SET_LOCATIONS',
    locations,
    loadingLocations: false
  }
}
export function setLoadingTideData(status) {
  return {
    type: 'SET_LOADING_TIDE_DATA',
    loadingTideData: status
  }
}
export function pushTideData(tideData) {
  return {
    type: 'PUSH_TIDE_DATA',
    tideData,
    loadingTideData: false
  }
}
export function setDateRange(dateRange) {
  return {
    type: 'SET_DATE_RANGE',
    dateRange: dateRange.map(date => date.format('YYYY-MM-DD'))
  }
}
export function loadLocations() {
  return {
    queue: 'loadLocationsQueue',
    callback: (next, dispatch, getState) => {
      dispatch(setLoadingLocations(true))
      fetch('./json/locations.json')
      .then(r => r.json())
      .then(r => {
        dispatch(setLocations(r))
        next()
      })
      .catch(e => {
        console.error(e)
        next()
      })
    }
  }
}
export function loadTideData() {
  return {
    queue: 'loadTideDataQueue',
    callback: (next, dispatch, getState) => {
      const state = getState()
      const locationId = state.get('locationId')
      if (!locationId) {
        // no location selected, can't fetch
        return next()
      }
      const dateRange = state.get('dateRange')
      if (!dateRange || dateRange.size !== 2) {
        // no dateRange selected, can't fetch
        return next()
      }
      let years = dateRange.map(d => d.substr(0, 4))
      const tideData = state.get('tideData')
      if (tideData) {
        const locationData = tideData.filter(td => td.get('id') === locationId)
        const year1Data = locationData.find(td => td.get('year') === years.get(0))
        if (years.get(0) === years.get(1)) {
          if (year1Data) {
            // data already fetched
            return next()
          }
          // don't need to fetch both years
        } else {
          const year2Data = locationData.find(td => td.get('year') === years.get(1))
          if (year1Data && year2Data) {
            // data already fetched
            return next()
          }
          // we don't have all the data we need
          // check if we have some of the data
          if (year1Data) {
            // we have year1, don't fetch it
            years = years.delete(0)
          } else if (year2Data) {
            // we have year2, don't fetch it
            years = years.delete(1)
          }
        }
      }
      if (years.get(0) === years.get(1)) {
        years = years.delete(1)
      }
      dispatch(setLoadingTideData(true))
      Promise.all(years.map(year => fetch(`./json/${locationId}-${year}.json`)).toJS())
      .then(result => Promise.all(result.map(r => r.json())))
      .then(result => {
        result.map(r => dispatch(pushTideData(r)))
        next()
      })
      .catch(e => {
        console.error(e)
        next()
      })
    }
  }
}
