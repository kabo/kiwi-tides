import {Map, fromJS} from 'immutable'
import moment from 'moment'
import reducer from './reducer'

it('handles SET_LOCATIONS', () => {
  const initialState = Map()
  const action = {type: 'SET_LOCATIONS', locations: [
    {id: 'placea', name: 'Place A'},
    {id: 'otherplace', name: 'Other place'}
  ], loadingLocations: false}
  const nextState = reducer(initialState, action)

  expect(nextState).toEqual(fromJS({locations: [
    {id: 'placea', name: 'Place A'},
    {id: 'otherplace', name: 'Other place'}
  ], loadingLocations: false}))
})
it('handles SET_LOCATION', () => {
  const initialState = Map()
  const action = {type: 'SET_LOCATION', locationId: 'location'}
  const nextState = reducer(initialState, action)

  expect(nextState).toEqual(fromJS({locationId: 'location'}))
})
it('handles SET_DATE_RANGE', () => {
  const initialState = Map()
  const action = {type: 'SET_DATE_RANGE', dateRange: ['2017-04-10', '2017-04-15']}
  const nextState = reducer(initialState, action)

  expect(nextState).toEqual(fromJS({dateRange: ['2017-04-10', '2017-04-15']}))
})
it('handles PUSH_TIDE_DATA', () => {
  const initialState = fromJS({tideData: []})
  const action = {type: 'PUSH_TIDE_DATA', tideData: {location: 'location', id: 'id', year: '2017', tides: [{datetime: 'datetime', height: '0.4'}]}}
  const nextState = reducer(initialState, action)

  expect(nextState).toEqual(fromJS({tideData: [{location: 'location', id: 'id', year: '2017', tides: [{datetime: 'datetime', height: '0.4'}]}], loadingTideData: false}))
})

