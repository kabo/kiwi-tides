import React, { PureComponent } from 'react'

export default class About extends PureComponent {
  render() {
    return (
      <div className="About">
        <p><a href="https://gitlab.com/kabo/kiwi-tides" target="_blank">Source code</a></p>
        <p>Kiwi tides is licensed under the terms of the MIT license.</p>
        <p>This work includes LINZ's data which are licensed by <a href="http://www.linz.govt.nz/" target="_blank">Land Information New Zealand (LINZ)</a> for re-use under the <a href="http://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International licence</a>.</p>
        <p>Disclaimer from LINZ:</p>
        <p>The tide predictions are not official tide tables as specified in Maritime Rules Part 25 Nautical Charts and Publications (pursuant to Section 36 of the Maritime Transport Act 1994).</p>
        <p>LINZ accepts no liability for any direct, indirect, consequential or incidental damages that result from any errors in the information, whether due to LINZ or a third party, or that arise from the use, or misuse, of the information available from this site.</p>
      </div>
    )
  }
}


