import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import {connect} from 'react-redux'
import {List} from 'immutable'
import ReactMapboxGl, { Layer, Feature, ScaleControl, ZoomControl } from 'react-mapbox-gl'
import { setLocationId, loadTideData } from './actions'
import {addReducer} from './reducer'

// highlight selected location
// larger map?

const setCenter = ({mapCenter}) => {
  return {
    type: 'SET_MAP_CENTER',
    mapCenter
  }
}
const setZoom = ({mapZoom}) => {
  return {
    type: 'SET_MAP_ZOOM',
    mapZoom
  }
}

addReducer({type: 'SET_MAP_CENTER', reducer: (state, action) => state.merge({mapCenter: action.mapCenter})})
addReducer({type: 'SET_MAP_ZOOM', reducer: (state, action) => state.merge({mapZoom: action.mapZoom})})

const MapboxMap = new ReactMapboxGl({
  accessToken: process.env.REACT_APP_MAPBOX_ACCESSTOKEN
})

class TideLocationMap extends Component {
  constructor(props, context) {
    super(props, context)
    this._markerClick = this._markerClick.bind(this)
    this._onToggleHover = this._onToggleHover.bind(this)
    this.props.setCenter({ mapCenter: [173.87378341221688, -41.00000000000000], })
    this.props.setZoom({ mapZoom: [3] })
  }
  _markerClick(location, { feature }) {
    const locationId = location.id
    this.props.setCenter({ mapCenter: feature.geometry.coordinates, })
    this.props.setZoom({ mapZoom: [14] })
    this.props.onChangeLocation(locationId)
    this.props.history.push(`/${locationId}`)
  }
  _onToggleHover(cursor, { map }) {
    map.getCanvas().style.cursor = cursor
  }
  render() {
    const locations = this.props.locations
    //const locationId = this.props.locationId
    const containerStyle = {
      height: '300px',
      width: '100%'
    }
    return (
      <div className="TideLocationMap" style={{width: '350px', height: '300px'}}>
        <MapboxMap
          style={'mapbox://styles/mapbox/outdoors-v10'} // eslint-disable-line react/style-prop-object
          movingMethod="jumpTo"
          containerStyle={containerStyle}
          center={this.props.mapCenter}
          zoom={this.props.mapZoom}
        >
          <ScaleControl/>
          <ZoomControl/>
          <Layer
            type="circle"
            id="marker"
            paint={{
              'circle-opacity': .8,
              'circle-color': 'brown',
              'circle-radius': 2,
              'circle-stroke-width': 1,
              'circle-stroke-color': '#1D1D16',
              'circle-stroke-opacity': .8
            }}
          >
            { locations
              .map(location => (
                <Feature
                  key={location.id}
                  onMouseEnter={(x) => this._onToggleHover('pointer', x)}
                  onMouseLeave={(x) => this._onToggleHover('', x)}
                  onClick={(x) => this._markerClick(location, x)}
                  coordinates={location.lnglat}/>
              ))
            }
          </Layer>
        </MapboxMap>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    locations: state.get('locations', new List()).toJS(),
    locationId: state.get('locationId'),
    loadingLocations: state.get('loadingLocations'),
    mapCenter: state.get('mapCenter', new List([173.87378341221688, -41.00000000000000])).toJS(),
    mapZoom: state.get('mapZoom', new List([3])).toJS(),
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onChangeLocation: (locationId) => {
      dispatch(setLocationId(locationId))
      dispatch(loadTideData())
    },
    setCenter: data => dispatch(setCenter(data)),
    setZoom: data => dispatch(setZoom(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TideLocationMap))



