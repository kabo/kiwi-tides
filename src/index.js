import React from 'react'
import ReactDOM from 'react-dom'
import {
  HashRouter as Router,
  Route
} from 'react-router-dom'
import store from './store'
import {Provider} from 'react-redux'
import moment from 'moment'
import { LocaleProvider } from 'antd'
import enUS from 'antd/lib/locale-provider/en_US'
import App from './App'
import {loadLocations, setDateRange, init} from './actions'
import './index.css'
import * as serviceWorker from './serviceWorker'

moment.locale('en-nz')
store.dispatch(init())
store.dispatch(loadLocations())
const dateFrom = moment().startOf('day')
const dateTo = dateFrom.clone().add(1, 'days').endOf('day')
store.dispatch(setDateRange([dateFrom, dateTo]))

ReactDOM.render(
  <Provider store={store}>
    <LocaleProvider locale={enUS}>
      <Router>
        <Route component={App} />
      </Router>
    </LocaleProvider>
  </Provider>,
  document.getElementById('root')
)
serviceWorker.unregister()
