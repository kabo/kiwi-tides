import {Map, fromJS} from 'immutable'

function setState(state, newState) {
  return state.merge(newState)
}
function pushTideData(state, tideData) {
  return state.merge({
    tideData: state.get('tideData').push(fromJS(tideData)),
    loadingTideData: false
  })
}
const extraReducers = {}
export const addReducer = ({type, reducer}) => {
  extraReducers[type] = reducer
}

export default function(state = new Map(), action) {
  switch (action.type) { // eslint-disable-line default-case
  case 'SET_STATE':
    return setState(state, action.state)
  case 'SET_LOCATIONS':
    return setState(state, {
      locations: action.locations,
      loadingLocations: action.loadingLocations
    })
  case 'SET_LOADING_LOCATIONS':
    return setState(state, {loadingLocations: action.loadingLocations})
  case 'SET_LOCATION':
    return setState(state, {locationId: action.locationId})
  case 'SET_DATE_RANGE':
    return setState(state, {dateRange: action.dateRange})
  case 'SET_LOADING_TIDE_DATA':
    return setState(state, {loadingTideData: action.loadingTideData})
  case 'PUSH_TIDE_DATA':
    return pushTideData(state, action.tideData)
  }
  if (extraReducers.hasOwnProperty(action.type)) {
    return extraReducers[action.type](state, action)
  }
  return state
}
