import React, { PureComponent } from 'react'
import { withRouter } from 'react-router-dom'
import {connect} from 'react-redux'
import {Popover, Row, Col, Icon} from 'antd'
import moment from 'moment'
import LocationPickerContainer from './LocationPicker'
import DateRangePickerContainer from './DateRangePicker'
import TideTableContainer from './TideTable'
import TideChartContainer from './TideChart'
import TideLocationMapContainer from './TideLocationMap'

class Tide extends PureComponent {
  render() {
    const locationId = this.props.locationId
    const dateRange = this.props.dateRange
    const tideData = this.props.tideData
    const loadingTideData = this.props.loadingTideData
    let locationData = false
    if (locationId && dateRange && tideData) {
      const years = dateRange.map(date => date.substr(0, 4))
      const fromDate = moment(dateRange.get(0)).startOf('day')
      const toDate = moment(dateRange.get(1)).endOf('day')
      locationData = tideData.filter(td => {
        return td.get('id') === locationId && years.indexOf(td.get('year') !== -1)
      }).map(td => {
        return td.get('tides').map(tide => {
          const m = moment(tide.get('datetime'))
          return tide.merge({datetime: m, key: m.toISOString()})
        })
      })
        .flatten(true)
        .filter(tide => tide.get('datetime').isBetween(fromDate, toDate))
        .sort((a, b) => a.get('datetime').diff(b.get('datetime')))
        .toJS()
    }
    const popovercontent = (<TideLocationMapContainer />)
    return (
      <div className="Tide">
        <Row gutter={16}>
          <Col xs={23} sm={11}>
            <LocationPickerContainer />
          </Col>
          <Col xs={1} sm={1}>
            <Popover
              placement="top"
              content={popovercontent}>
              <Icon type="global" style={{fontSize: '16px', margin: '7px 0 0 0'}}/>
            </Popover>
          </Col>
          <Col xs={24} sm={12}>
            <DateRangePickerContainer />
          </Col>
        </Row>
        {locationData.length > 0 ? (
          <div style={{marginTop: '10px'}}>
            <TideChartContainer tideData={locationData} />
            <TideTableContainer tideData={locationData} />
          </div>
        ) : (<div style={{fontSize: '24px', color: 'gray', marginTop: '20px'}}>
          {loadingTideData ? <div><Icon type="loading" /> Loading...</div> : 'Select a location'}</div>)}
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    tideData: state.get('tideData'),
    locationId: state.get('locationId'),
    dateRange: state.get('dateRange'),
    loadingTideData: state.get('loadingTideData')
  }
}

export default withRouter(connect(mapStateToProps)(Tide))

