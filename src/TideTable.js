import React, { PureComponent } from 'react'
import { withRouter } from 'react-router-dom'
import {connect} from 'react-redux'
import { Table } from 'antd'
import moment from 'moment'
import './TideTable.css'

// highlight the tide closest to now

class TideTable extends PureComponent {
  render() {
    const tideData = this.props.tideData // from actual props, not state
    const now = moment()
    const columns = [
      {
        title: 'Time',
        dataIndex: 'datetime',
        render: (dt, o) => <span title={o.key} style={{fontWeight: (Math.abs(dt.diff(now, 'seconds')) < 60*60*3 ? 'bold' : 'normal')}}>{dt.format('ddd HH:mm')}</span>,
        sorter: (a, b) => {
          if (a.datetime.isBefore(b.datetime)) {
            return -1
          }
          if (a.datetime.isAfter(b.datetime)) {
            return 1
          }
          return 0
        }
      },
      {
        title: 'Height',
        dataIndex: 'height',
        render: (height, o) => <span style={{fontWeight: (Math.abs(o.datetime.diff(now, 'seconds')) < 60*60*3 ? 'bold' : 'normal')}}>{height}</span>,
        sorter: (a, b) => a.height - b.height
      }
    ]
    return (
      <div className="TideTable">
        <Table 
          className="Tides"
          columns={columns}
          dataSource={tideData}
          pagination={false}
          footer={() =>
            <span style={{fontSize: '.9em', fontStyle: 'italic'}}>
              Bold time is closest to current time. Red line in chart marks current time.
            </span>
          }
        />
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return { }
}

export default withRouter(connect(mapStateToProps)(TideTable))


