import React, { PureComponent } from 'react'
import { withRouter } from 'react-router-dom'
import {connect} from 'react-redux'
import {setDateRange, loadTideData} from './actions'
import { DatePicker, Button, Row, Col } from 'antd'
import moment from 'moment'
const { RangePicker } = DatePicker

const nextDay = (props) => {
  const fromDate = moment(props.dateRange.get(0)).startOf('day').add(1, 'day')
  const toDate = moment(props.dateRange.get(1)).endOf('day').add(1, 'day')
  props.onChangeDate([fromDate, toDate])
}
const prevDay = (props) => {
  const fromDate = moment(props.dateRange.get(0)).startOf('day').subtract(1, 'day')
  const toDate = moment(props.dateRange.get(1)).endOf('day').subtract(1, 'day')
  props.onChangeDate([fromDate, toDate])
}

class DateRangePicker extends PureComponent {
  render() {
    const fromDate = moment(this.props.dateRange.get(0)).startOf('day')
    const toDate = moment(this.props.dateRange.get(1)).endOf('day')
    return (
      <div className="DatePicker">
        <Row>
          <Col span={2}>
            <Button style={{width: '100%'}} onClick={() => prevDay(this.props)} disabled={!fromDate || !toDate}>&laquo;</Button>
          </Col>
          <Col span={20}>
            <RangePicker
              value={[fromDate, toDate]}
              onChange={this.props.onChangeDate}
              style={{width: '100%'}}
              />
          </Col>
          <Col span={2}>
            <Button style={{width: '100%'}} onClick={() => nextDay(this.props)} disabled={!fromDate || !toDate}>&raquo;</Button>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    dateRange: state.get('dateRange')
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onChangeDate: (moments, strings) => {
      dispatch(setDateRange(moments))
      dispatch(loadTideData())
    }
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DateRangePicker))



