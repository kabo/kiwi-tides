import React, { PureComponent } from 'react'
import { withRouter } from 'react-router-dom'
import {connect} from 'react-redux'
import RC2 from 'react-chartjs2'
import 'chartjs-plugin-annotation'
import moment from 'moment'

class TideChart extends PureComponent {
  render() {
    const tideData = this.props.tideData // from actual props, not state
    const fromDate = moment(this.props.dateRange.get(0)).startOf('day')
    const toDate = moment(this.props.dateRange.get(1)).endOf('day')
    const chartData = {
      datasets: [{
        label: 'Height (m)',
        backgroundColor: 'rgba(85, 144, 252, .5)',
        borderColor: 'rgba(85, 144, 252, .8)',
        data: tideData.map(tide => {
          return {
            x: tide.datetime,
            y: tide.height
          }
        })
      }]
    }
    const chartOptions = {
      maintainAspectRatio: false,
      animation: {
        duration: 0
      },
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            //unit: 'day',
            displayFormats: {
              minute: 'dd HH:mm',
              hour: 'dd HH:mm',
              day: 'ddd',
              week: 'ddd'
            }
          }
        }]
      }
    }
    const now = moment()
    if (now.isBetween(fromDate, toDate)) {
      chartOptions.annotation = {
        annotations: [{
          id: 'now-line',
          type: 'line',
          mode: 'vertical',
          borderColor: 'red',
          borderWidth: 1,
          scaleID: 'x-axis-0',
          value: now
        }]
      }
    }
    return (
      <div className="TideChart">
        <RC2 height="200" data={chartData} options={chartOptions} type="line" />
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    dateRange: state.get('dateRange')
  }
}

export default withRouter(connect(mapStateToProps)(TideChart))



