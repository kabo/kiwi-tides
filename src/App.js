import React, { Component } from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import TideContainer from './Tide'
import About from './About'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2 style={{display: 'inline'}}><Link to="/">Tides</Link></h2>
          <nav style={{display: 'inline'}}>
            <ul style={{display: 'inline'}}>
              <li>
                <Link to="/about">About</Link>
              </li>
            </ul>
          </nav>
        </div>
        <div className="App-content">
          <Switch>
            <Route path="/about" component={About} />
            <Route path="/:locationId" component={TideContainer} />
            <Route path="/" component={TideContainer} />
          </Switch>
        </div>
      </div>
    )
  }
}

export default App
