import React, { PureComponent } from 'react'
import { withRouter } from 'react-router-dom'
import {connect} from 'react-redux'
import {List} from 'immutable'
import { setLocationId, loadTideData } from './actions'
import { Select } from 'antd'
const Option = Select.Option

class LocationPicker extends PureComponent {
  componentWillMount() {
    const locationId = this.props.match.params.locationId
    if (locationId) {
      this.props.onChangeLocation(locationId)
    }
  }
  filterLocations(input, option) {
    const lower = input.toLowerCase()
    return option.props.value.toLowerCase().includes(lower) || option.props.children.toLowerCase().includes(lower)
  }
  render() {
    const locations = this.props.locations
    const locationId = this.props.locationId
    const loadingLocations = this.props.loadingLocations
    return (
      <div className="LocationPicker">
        <Select
          showSearch
          placeholder="Select a location"
          notFoundContent={loadingLocations?"Loading locations...":"Unknown location"}
          value={locationId}
          filterOption={this.filterLocations}
          onChange={locationId => {
            this.props.onChangeLocation(locationId)
            this.props.history.push(`/${locationId}`)
          }}
          style={{width: '100%'}}
          >
          {locations.map(loc => {
            return <Option key={loc.get('id')} value={loc.get('id')}>{loc.get('location')}</Option>
          }).toJS()}
        </Select>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    locations: state.get('locations', new List()),
    locationId: state.get('locationId'),
    loadingLocations: state.get('loadingLocations')
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onChangeLocation: (locationId) => {
      dispatch(setLocationId(locationId))
      dispatch(loadTideData())
    }
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LocationPicker))


